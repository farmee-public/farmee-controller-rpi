# farmee installation

- Register at https://brain.farmee.io to obtain an API key
- Checkout or download the sources from https://gitlab.com/farmee/farmee-controller-rpi
- Set execute rights to install.sh: `chmod +x install.sh`
- Execute startup script `./install.sh`
- Enter your API key and an among your units unique identifier (UUID), eg. MyNiceUnit1
