# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


import logging
import time
from threading import Timer

from logic.LogicBase import LogicBase


class Regulator(LogicBase):
    """
    Unused at this point. Use at own risk, it is very likely to crash the application.

    Regulator Element. See http://farmee.io:8081/browse/FOS-43.
    The input value from a defined sensor is checked.
    If the trigger condition evaluates to true, the trigger is
    triggered for a defined time. After that the logic element sleeps
    for a given time.

    Configuration Example JSON:
        {
            "name": "ph-regulation"
            "type":"Regulation",
            "config": {
                "sensor": "ph",
                "sensor-value": "ph",
                "actor": "phcontrol",
                "trigger-condition":">7",
                "trigger-duration":10,
                "sleep-duration": 60
            }
        }
    """

    def __init__(self):
        super(Regulator,self).__init__()
        self.sensorValue = ""
        self.triggerCondition = ""
        self.triggerDuration = 0
        self.sleepDuration = 0
        self.checkInterval = 10
        self.initialized = 0
        self.nextCheckTime = 0
        return


    def init(self,config,sensorList,actorList):
        self.actorName = config["actor"]
        self.sensorName = config["sensor"]

        initialized = True

        if self.sensorName in sensorList:
            self.sensor = sensorList[ self.sensorName ]
        else:
            logging.error( "regulator init error: sensor %s not found in sensor list", self.sensorName )
            initialized = False

        if self.actorName in actorList:
            self.actor = actorList[ self.actorName ]
        else:
            logging.error( "regulator init error: actor  %s not found in actor list", self.actorName )
            initialized = False

        self.sensorValue = config["sensor-value"]
        self.triggerCondition = config["trigger-condition"]
        self.triggerDuration = config["trigger-duration"]
        self.sleepDuration = config["sleep-duration"]

        self.initialized = initialized

        return

    def switchOffActor(self):
        logging.info( "regulator stop actor %s", self.actorName )
        self.actor.input( False )
        return

    def process(self):
        logging.info( "regulator process" )
        if( self.initialized ):

            currentTime = int(time.time())
            timediff = int(round( currentTime - self.nextCheckTime ))

            logging.info( "regulator timecheck %s", timediff )

            if( timediff > 0 ):
                # read sensor value
                value = self.sensor.read( self.sensorValue )

                # concatenate value and condition to one string (e.g. 6.45>7)
                condition = str(value) + self.triggerCondition
                # then eval that string
                result = eval( condition )
                # and check the evaled result
                if( result == True ):
                    logging.info( "regulator trigger actor %s due to condition: %s", self.actorName, condition )
                    logging.info( "regulator trigger for %s seconds", self.triggerDuration )
                    # trigger actor and write next check time + sleep
                    self.actor.input( True )
                    # stop actor after set duration
                    thread = Timer( self.triggerDuration, self.switchOffActor )
                    thread.start();
                    # set next check time with sleep duration
                    self.nextCheckTime = currentTime + self.sleepDuration
                else:
                    # trigger next check after interval
                    self.nextCheckTime = currentTime + self.checkInterval

        return
