# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


class Comparator():

    outputs = []
    high = 0
    low = 0
    value = 0
        
    def addOutput(self,o):
        self.outputs.append( o )
        return
    
    def setHighValue(self,h):
        self.high = h
        return
    
    def setLowValue(self,l):
        self.low = l
        return
        
    def input(self,v):
        self.value = v
        self.process()
        return
    
    def process(self):
        trigger = False
        if self.value >= self.low and self.value <= self.high:
            trigger = True
            
        for o in self.outputs:
            o.input( trigger )
        return
