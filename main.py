# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function, unicode_literals
from builtins import input

import argparse
import hashlib
import importlib
import json
import logging
import time
import platform
import socket

from os.path import isfile
from time import sleep

from lib.yocto.yocto_api import *

import RPi.GPIO as GPIO
import sys
from datetime import datetime
from tzlocal import get_localzone

from config.file import FileConfiguration
from config.server import ServerConfiguration
from connector.ApiConnector import ApiConnector
from runners.TimerRunner import TimerRunner
from sensors.SensorMonitor import SensorMonitor

# filenames for config and control relative to the main.py
config_filename = 'config.json'
control_filename = 'control.json'

# hashed config to check if it changed
currentConfigHash = ''

# initialize empty arrays/dictionaries
sensorList = {}
actorList = {}
actorById = {}
logicList = {}
monitors = []
executedEvents = []

# handling of timed actions
timerRunner = TimerRunner()

# http connection handling
connector = ApiConnector()

# state of Yocto API inizialization
YAPIinitialized = False

# current unit id
unitId = 0

refresh = None

def merge_configurations(file_config, server_config):
    logging.info('File time %s' % file_config.last_change)
    if( server_config.last_change == False ):
        logging.warning( 'no server config found, configs will not be merged' )
    else:
        logging.info('Server time %s' % server_config.last_change)

        if server_config.last_change < file_config.last_change:
            logging.info( 'Merge file to server' )
            server_config.merge(file_config)
            server_config.save()
            tz = get_localzone()
            file_config.last_merge = datetime.now(tz=tz)
        else:
            logging.info( 'Merge server to file' )
            file_config.merge(server_config)
            file_config.save()

    return file_config


def read_configuration(config_only):
    global unitId

    file_config = FileConfiguration(config_filename)

    # reset configuration
    if config_only:
        file_config.api_key = ""
        file_config.uuid = ""

    # @TODO: Validate structure of config file

    # check if api key is set, if not, ask user for api key
    while file_config.api_key == "":
        print( 'No API key found in config file. Please provide your API key now. If you don\'t have an API key yet'
            + ' please go to https://cloud.farmee.io and create an account.' )
        apiKey = input('Enter your API key: ')
        file_config.api_key = apiKey
        file_config.save()

    # check if unit uuid is set, if not, ask user for uuid
    while file_config.uuid == "":
        print( 'No UUID found in config file. Please set a identifier that is unique among your units.' )
        uuid = input('Enter your UUID: ')
        file_config.uuid = uuid
        file_config.save()

    connected = connector.init(file_config.host, file_config.api_key, file_config.port)
    if( connected == False ):
        print( 'Error: Connection failed.' )
        sys.exit()

    # register unit
    plat = platform.system() + ' ' + platform.release()
    ip = getDeviceIpAddress()
    type = 'farmee controller rpi'
    # @TODO: get version from .version file
    version = '0.2.0'

    unitId = connector.registerUnit( uuid=file_config.uuid,
                                     platform=plat,
                                     ip=ip,
                                     type=type,
                                     version=version )

    logging.info( 'Current Platform: %s', plat )
    logging.info( 'Current IP: %s', ip )
    logging.info( 'Current Version: %s', version )

    if config_only:
        print( 'Configuration completed. Exiting.' )
        sys.exit()

    # then read configuration with unit id
    server_config = ServerConfiguration(connector,unitId)
    if( server_config.last_change == False ):
        logging.warning( 'No server configuration found, please add a configuration in farmee.' )

    return merge_configurations(file_config, server_config)

def getDeviceIpAddress():
    """
    Gets the IP address that has access to the internet. Therefore we initialize a connection to
    a public DNS server.

    For details see:
    https://stackoverflow.com/questions/166506/finding-local-ip-addresses-using-pythons-stdlib

    :return: string
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()
    return ip

def init(config):
    """
    Initialization after application start

    :return: bool if reading config was successful
    """
    global currentConfigHash
    global unitId
    global refresh

    if refresh is None:
        refresh = config["refresh"]

    logging.info("Initializing with unit id %s, refreshing every %d secs", unitId, refresh)

    if not isfile(control_filename):
        logging.error("control.json not found. local mode cannot run without control json. exiting.")
        return False

    initSensors(config.sensors)
    initActors(config.actors)

    try:
        with open(control_filename) as data_file:
            control_config = json.load(data_file)
    except ValueError as e:
        logging.error('Could not read controls config')
        logging.error(e)
        return False

    updateControlConfiguration(control_config)
    currentConfigHash = hashlib.md5(json.dumps(control_config)).hexdigest()

    return True

def initializeYAPI():
    """
    Setup the Yocto API to use local USB devices
    """
    global YAPIinitialized

    errmsg = YRefParam()
    if YAPI.RegisterHub("usb", errmsg) != YAPI.SUCCESS:
        logging.error("Yocto USB init error " + errmsg.value)
    else:
        YAPIinitialized = True
        logging.info("Yocto USB initialized")


def initSensors(config):
    """
    Initialize sensors and monitors from json

    :param config: map with config settings
    """
    global sensorList
    global monitors
    global YAPIinitialized

    # reset current sensor and monitor list
    for i,delSens in sensorList.items():
        delSens.stop()
        del delSens

    for delMon in monitors:
        del delMon

    sensorList = {}
    monitors = []

    for sensor in config:
        sensor_type = sensor["type"]

        if sensor_type.find("Yocto") != -1 and YAPIinitialized == False:
            initializeYAPI()

        if sensor_type is None:
            logging.error('Sensor configuration has errors, type cannot be empty')
            continue

        logging.info("import sensor %s" % sensor_type)
        fullname = "sensors." + sensor_type

        try:
            classname = getattr(importlib.import_module(fullname), sensor_type)
            logging.debug('Trying to load sensor %s', classname)
            sensor_instance = classname(sensor['name'])
        except ImportError as e:
            logging.error("Sensor of type %s not supported, check configuration", sensor_type)
            logging.debug(e)
            continue

        sensor_config = sensor["config"]

        try:
            sensor_instance.init(sensor_config)
            sensor_instance.start()

            sensorList[sensor["id"]] = sensor_instance

            # init monitor, if sensor should be monitored
            if "monitor" in sensor and sensor["monitor"] == 1:
                v = sensor_config["reading"]
                if v != '':
                    logging.info("init monitor for %s %s", sensor["name"], v)
                    sm = SensorMonitor()
                    sm.init(sensor=sensor_instance,
                            value=v, name=sensor["name"],
                            sensorId=sensor["id"])
                    monitors.append(sm)
                else:
                    logging.warning( 'reading for %s is not set. check your configuration', sensor["name"] )

        except IOError as e:
            logging.error('Could not initialize sensor')
            logging.error(e)

    logging.info("List of configured sensors")
    [logging.info("- %s", sensor.name) for i,sensor in sensorList.items()]


def initActors(config):
    """
    Initialize actors from json

    :param config: map with actor configuration
    """
    global actorList
    global actorById
    global YAPIinitialized

    # reset current actor list
    for act in config:
        act["exists"] = False

    # check which actuators should be kept
    for id,actOld in actorById.items():
        for act in config:
            if act["id"] == id:
                # keep actuator
                logging.info("keep actuator %s", act["name"])
                act["exists"] = True
                act["instance"] = actOld

    actorList = {}

    for act in config:
        actor_type = act["type"]
        actor_name = act["name"]
        actor_config = act["config"]

        if act["exists"] == True:
            logging.info("dont recreate actuator %s", actor_name)
            actor_instance = act["instance"]
            actor_instance.init(actor_config)
            actorList[actor_name] = actor_instance

            del act["exists"]
            del act["instance"]

            continue

        logging.info("import actor " + actor_type)

        if actor_type.find("Yocto") != -1 and YAPIinitialized == False:
            initializeYAPI()

        try:
            classname = getattr( importlib.import_module('actors.actors'), actor_type)
            actor_instance = classname()

            actor_instance.init(actor_config)
            actor_instance.name = act["name"]
            actor_instance.id = act["id"]

            actorList[actor_name] = actor_instance
            if "id" in act:
                actorById[int(float(act["id"]))] = actor_instance
            else:
                logging.warning( 'actor %s has no id', act["name"] )

        except ImportError as e:
            logging.error("Actor of type %s not supported, check configuration", actor_type)
            logging.debug(e)

    logging.info("List of configured actors")
    [logging.info("- %s", actor) for actor in actorList]


def initLogic(config):
    """
    Initialize logic elements from json

    :param config:
    :return:
    """
    global logicList
    global sensorList
    global actorList

    logging.info("update logic configuration")

    # reset current logic list
    logicList = []

    for logic in config:
        logictype = logic["type"]

        fullname = "logic." + logictype
        try:
            classname = getattr( importlib.import_module(fullname), logictype )
            logicInstance = classname()
        except ImportError as e:
            logging.error("Sensor of type %s not supported, check configuration", logictype)
            continue

        logicConfig = logic["config"]
        logicInstance.init(logicConfig, sensorList, actorList)

        logicList.append(logicInstance)
    return


# initialize timers
def initTimers( conf ):
    global timerRunner
    global actorList

    timerRunner.initTimers( conf, actorList )
    return

def processEvents( events ):
    """
    events can switch actuators for a time or permanently
    """

    global actorById
    global timerRunner
    global executedEvents

    for event in events:
        type = event["type"]
        value = event["value"]
        actuator_id = int(float(event["actuatorId"]))
        duration = False
        if "duration" in event:
            duration = event["duration"]

        # get actuator from list
        if actuator_id in actorById:
            actuator = actorById[actuator_id]
            actuator_name = actuator.name

            logging.info( 'process event %s for actuator %s', type, actuator_name )

            if type == 'switch':
                if value == 'ON':
                    if duration != False:
                        logging.info( 'switch actuator %s to ON for %s seconds', actuator_name, duration )
                        actuator.override( True, duration )
                    else:
                        logging.info('switch actuator %s to ON', actuator_name )
                        actuator.override( True )

                elif value == 'OFF':
                    if duration != False:
                        logging.info( 'switch actuator %s to OFF for %s seconds', actuator_name, duration )
                        actuator.override( False, duration )
                    else:
                        logging.info('switch actuator %s to OFF', actuator_name )
                        actuator.override( False )
                elif value == 'AUTO':
                    logging.info( 'switch actuator %s to AUTO', actuator_name )
                    actuator.enableAutomatic()

            event["executedAt"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

            # add to list of executed events
            executedEvents.append( event )

    return


# updates the current control configuration with a new config
def updateControlConfiguration(config):
    logging.info("Updating control configuration")
    logging.debug(config)
    initTimers(config["timers"])
    if "events" in config:
        processEvents(config["events"])

    # if "logic" in config:
    #    initLogic(config["logic"])
    return


# saves a control config json to the disk
def saveControlConfiguration(config):
    global control_filename

    f = open(control_filename, 'w+')
    json.dump(config, f)
    return


def update_configuration(configuration):

    global currentConfigHash

    configHash = hashlib.md5(configuration).hexdigest()
    if currentConfigHash != configHash:
        currentConfigHash = configHash
        try:
            configJson = json.loads(configuration)
            updateControlConfiguration(configJson)
            saveControlConfiguration(configJson)

        except ValueError as e:
            logging.warning("configuration could not be parsed: %s", e)


def readUnitstate():
    """
    reads the current sensor values and submits them to the control unit
    :return:
    """
    global unitId
    global actorList
    global executedEvents

    sensor_list = []
    for m in monitors:
        sensor_list.append({
            'sensorId': m.getSensorId(),
            'name': m.getName(),
            'reading': m.getValue(),
            'value': m.read()
        })

    actuator_list = []
    for i, actor in actorList.items():
        actuator_list.append({
            'actuatorId': actor.id,
            'currentValue': actor.value
        })

    event_list = []
    for e in executedEvents:
        print(e)
        event_list.append( {
            'eventId': e["id"],
            'executedAt': e["executedAt"]
        })

    # clear list of executed events
    executedEvents = []

    st = json.dumps({"sensors": sensor_list, "actuators": actuator_list, "events": event_list})
    state = {"unitId": unitId, "time": time.strftime("%Y-%m-%dT%H:%M:%S+02:00"), "state": st}
    logging.info('Sending unit state to server')
    newConfiguration = connector.updateState(state)

    if newConfiguration is False:
        logging.warning("No configuration received from server")
    else:
        update_configuration(newConfiguration)


def init_logging(logfile, loglevel):
    levels = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warn': logging.WARNING,
        'error': logging.ERROR
    }

    config = {
        'level': levels[loglevel]
    }

    if logfile is not None:
        config['filename'] = logfile

    config['level'] = levels[loglevel]

    logging.basicConfig(**config)


def get_args():
    choices = ['debug', 'info', 'warn', 'error']
    parser = argparse.ArgumentParser(description='Farmee Controller')
    parser.add_argument('--logfile', '-f', help='the name of the log file to write to')

    parser.add_argument('--loglevel', '-l',
                        help='the log level, either DEBUG, WARN or ERROR',
                        choices=choices,
                        default='info')

    parser.add_argument('--refresh', '-r',
                        help='Interval how often the sensor values should be reported',
                        type=int,
                        default=15)

    parser.add_argument('--config', '-c',
                        help='Only start config mode and exit afterwards',
                        dest='config',
                        action='store_true')
    parser.set_defaults(config=False)

    return parser.parse_args()


def updateConfigurationFromServer(file_config):

    global unitId

    server_config = ServerConfiguration(connector,unitId)

    logging.debug('File last_change is %s' % file_config.last_change)

    if server_config.config is not False:
        logging.debug('Server last_change is %s' % server_config.last_change)

        logging.debug("server hash: %s", server_config.hash)
        logging.debug("file hash: %s", file_config.hash)

        if file_config.last_merge == server_config.last_change or file_config.hash == server_config.hash:
            logging.debug('Configuration still the same, nothing to do')
            return file_config

        logging.info('Configuration on server changed. Updating...')
        file_config.merge(server_config)
        file_config.save()

        initSensors(file_config.sensors)
        initActors(file_config.actors)
    else:
        logging.debug('Server config could not be read. Keeping current config.')

    return file_config


if __name__ == '__main__':

    args = get_args()
    init_logging(args.logfile, args.loglevel)
    if args.refresh is not None:
        refresh = args.refresh

    # show gpl notice
    print('farmee controller, Copyright (C) 2018  farmee\n' +
          'farmee controller comes with ABSOLUTELY NO WARRANTY.\n' +
          'This is free software, and you are welcome to redistribute it\n' +
          'under certain conditions.\n\n')

    # initialize the RPi GPIO Module
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    # read the current configuration
    config = read_configuration(args.config)

    # initialize the application
    initialized = init(config)

    # start threaded runners
    timerRunner.start()

    if initialized:
        logging.info("Application initialized, starting main loop")

        # this is our main loop
        try:
            while True:
                s = time.strftime('%S')
                if int(s) % args.refresh == 0:
                    logging.info("Read unit states")
                    readUnitstate()

                    config = updateConfigurationFromServer(config)

                # process all logic elements
                for l in logicList:
                    logging.debug("Work in logic list")
                    l.process()

                sleep(1)
        except KeyboardInterrupt:
            print('Shutting down...')
            GPIO.cleanup()
            YAPI.FreeAPI()
            sys.exit()

    else:
        logging.error("Could not initialize application")
