# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


from threading import Thread
from time import sleep


class USBCameraReader(Thread):
    """
    Use at own risk
    """

    resW = 1280
    resH = 800

    def __init__(self):
        super(USBCameraReader,self).__init__()
        self.setDaemon( True )
        self.interval = 300
        return

    def run(self):
        while True:
            call(["fswebcam", "-d","/dev/video0", "-r", "1280x800", "--no-banner", "./usbimg.jpg"])
            sleep(1)
            with open("./usbimg.jpg", "rb") as image_file:
                b64value = base64.b64encode(image_file.read())

            # self.connector.sendData( 'image', b64value )
            sleep( self.interval )

        return

    def init(self,config):
        self.resW = config["resolution"]["w"]
        self.resH = config["resolution"]["h"]
        self.interval = config["interval"]
        return
