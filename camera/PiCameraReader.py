# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


from picamera import PiCamera
from threading import Thread
from time import sleep
from _io import BytesIO
import base64


class PiCameraReader(Thread):
    """
    Use at own risk
    """
    
    def __init__(self):
        super(PiCameraReader, self).__init__()
        self.setDaemon(True)
        self.interval = 60
        self.connector = False
        self.camera = None
        return
    
    def run(self):
        stream = BytesIO 
        while True:
            self.camera.capture( stream, 'jpeg' )
            content = stream.readall()
            b64value = base64.b64encode( content )
            self.connector.sendData('image', b64value)
            sleep(self.interval)

    def init(self, config):
        self.camera = PiCamera()
        self.camera.resolution = ( config["resolution"]["w"], config["resolution"]["h"] )
        self.interval = config["interval"]

    def setConnector(self, c):
        self.connector = c
