# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


"""
Contains all actor classes
"""

from __future__ import absolute_import, print_function, unicode_literals

import abc
import RPi.GPIO as GPIO
import logging
from datetime import datetime as dt

from lib.yocto.yocto_relay import *

class ActorBase(object):
    """
    Base class for all actors providing a method for configuration and
    for inputs
    """

    auto = True
    ignoreInputUntil = False
    _name = ""
    _id = 0
    _value = 0
    _initialized = False

    @abc.abstractmethod
    def init(self, config):
        pass

    @abc.abstractmethod
    def trigger(self, v):
        pass

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self,value):
        self._id = value

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def value(self):
        return self._value

    @property
    def is_auto(self):
        return self.auto

    def input(self, v, override=False):
        if self.auto == False and override == False:
            logging.info( '%s: skip input due to manual override', self.name )
            return

        currentTime = dt.now().time()
        if self.ignoreInputUntil != False and self.ignoreInputUntil > currentTime:
            logging.info('%s: skip input due to manual time override until %d', self.name, self.ignoreInputUntil )
            return

        self._value = 1 if v == True else 0

        self.trigger( v )

    def override(self, value, duration=False):
        if duration == False:
            self.auto = False
            self.input( value, True )
        else:
            currentTime = dt.now().time()
            self.ignoreInputUntil = currentTime + duration
            self.input( value, True )

        return

    def enableAutomatic(self):
        self.auto = True
        return


class SwitchGPIO(ActorBase):
    """
    GPIO base class
    Handles all actors that are connected via GPIO interface
    """

    def __init__(self):
        self.pin = 0
        self.switch = "high"

    def init(self, config):

        pin = config["gpio-pin"]
        switch = "high"
        if "switchlevel" in config:
            switch = config["switchlevel"]

        if switch != "high" and switch != "low":
            logging.warning("wrong switch level %d on pin %d. falling back to high", self.switch, self.pin)
            switch = "high"

        # only change pin if changed in config
        if self._initialized == False or pin != self.pin:
            self.pin = pin
            logging.info("initialize GPIO pin %d switchlevel %s", pin, self.switch)

            GPIO.setup(self.pin, GPIO.OUT)
        else:
            logging.info("dont reinitialize GPIO pin %d", pin)

        # only change default level if switchlevel changes
        if self._initialized == False or self.switch != switch:
            self.switch = switch
            if self.switch == "high":
                GPIO.output(self.pin,GPIO.LOW)
            else:
                GPIO.output(self.pin,GPIO.HIGH)

            # if already initialized, then switch according to last state
            if self._initialized:
                if self._value == 1:
                    self.trigger(True)
                else:
                    self.trigger(False)

        self._initialized = True
        return


    def trigger(self, v):
        if v is True:
            if self.switch == "high":
                state = GPIO.HIGH
            else:
                state = GPIO.LOW
        elif v is False:
            if self.switch == "high":
                state = GPIO.LOW
            else:
                state = GPIO.HIGH
        else:
            print("unsupported input %s " % v)

        try:
            logging.debug("SwitchGPIO trigger pin %d value %d", self.pin, state)
            GPIO.output(self.pin, state)
        except RuntimeError as e:
            logging.error(e)


class FanSimple(SwitchGPIO):
    """
    Simple fan GPIO actor
    """

    def getType(self):
        return "Fan"


class LightSimple(SwitchGPIO):
    """
    Simple light GPIO actor
    """

    def getType(self):
        return "Light"


class WaterSimple(SwitchGPIO):
    """
    Simple water GPIO actor
    """

    def getType(self):
        return "Water"

class YoctoMaxiPowerRelay(ActorBase):
    """
    Yoctopuce MaxiPowerRelay
    """

    def __init__(self):
        self.serial = 0
        self.channel = 0

    def init(self, config):
        self.serial = config["serial"]
        self.channel = config["channel"]

        self.relay = YRelay.FindRelay(self.serial + '.relay' + self.channel)

        if not (self.relay.isOnline()):
            logging.error("Yocto Relay %s Channel %s not connected",self.serial,self.channel)

        # if already initialized, then switch according to last state
        if self._initialized:
            if self._value == 1:
                self.trigger(True)
            else:
                self.trigger(False)

        self._initialized = True

    def trigger(self, v):
        if self.relay.isOnline():
            if v is True:
                self.relay.set_output(YRelay.OUTPUT_ON)
            else:
                self.relay.set_output(YRelay.OUTPUT_OFF)
        else:
            logging.error("Yocto Relay %s not connected",self.serial)

    def getType(self):
        return "YoctoMaxiPowerRelay"
