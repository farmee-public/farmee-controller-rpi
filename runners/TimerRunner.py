# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


# -*- coding: utf-8 -*-

from threading import Thread
from time import sleep
from timers.TimerSwitch import TimerSwitch
from timers.TimerInterval import TimerInterval
import logging


class TimerRunner(Thread):

    def __init__(self):
        super(TimerRunner, self).__init__()
        self.setDaemon(True)
        self.timers = []
        return
    
    def run(self):
        while True:
            for t in self.timers:
                t.trigger()
                
            sleep(1)

    def addTimer(self, timer):
        self.timers.append(timer)
        return
    
    def clearTimers(self):
        self.timers = []
        return

    def initTimers(self, conf, actor_list):
        # reset current timer list
        self.clearTimers()

        # parse all timers in config
        for timer in conf:

            actor_name = timer['event']['actor']

            if actor_name not in actor_list:
                logging.warning("Got control configuration for non existing actor: %s" % actor_name)
                logging.debug('Available actors: %s' % str(actor_list))
                continue

            if timer["type"] == "switch":
                t = TimerSwitch()
                t.setTimeConfig(timer["time"]["start"], timer["time"]["end"])
                actor = actor_list[actor_name]
                t.addOutput(actor)
                self.addTimer(t)

            elif timer["type"] == "interval":
                t = TimerInterval()
                t.setTime(timer["time"]["hour"], timer["time"]["minute"])
                t.setDuration(timer["event"]["duration"])
                ac2 = actor_list[actor_name]
                t.addOutput(ac2)
                self.addTimer(t)
            else:
                logging.warning('Wrong timer type "%s"' % timer['type'])
