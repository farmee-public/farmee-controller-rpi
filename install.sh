#!/usr/bin/env bash
CONTROLLER_FOLDER=`pwd`
PATTERN_CONTROLLER_FOLDER="CONTR_FOLDER_PATTERN"
echo "Install farmee controller... "
echo "Checking environment..."
# check python present

if ! [ -x "$(command -v python)" ]; then
  echo 'Error: python is not installed.' >&2
  exit 1
fi

# output python version
PYTHON_VERSION=`python -V`
echo ${PYTHON_VERSION}

# install requirements
echo "Install requirements..."
sudo apt-get -y install python-pip python-smbus
sudo pip install tzlocal python-dateutil future

# update startscript to correct path
echo "Install start script..."
cat ${CONTROLLER_FOLDER}/autostart/farmee.service.dist | sed -e "s|$PATTERN_CONTROLLER_FOLDER|$CONTROLLER_FOLDER|g" > ${CONTROLLER_FOLDER}/autostart/farmee.service

# copy startscript
sudo cp ${CONTROLLER_FOLDER}/autostart/farmee.service /lib/systemd/system/
# enable script
sudo systemctl enable farmee.service

# start in config mode
echo "Start configuration..."
python ./main.py --config

# start service
echo "Start service..."
sudo systemctl start farmee.service

echo "Installation completed. Farmee is now running on your system."
