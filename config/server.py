# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


from datetime import datetime
from pytz import timezone


class ServerConfiguration(object):

    def __init__(self, connector, unitId):

        self.connector = connector
        self.config = connector.getConfigurations(unitId)
        self.last_change = False

        if self.config is not False:
            tz = timezone(self.config['date_updated']['timezone'])
            self.last_change = tz.localize(datetime.strptime(self.config['date_updated']['date'], '%Y-%m-%d %H:%M:%S.%f'))

    @property
    def sensors(self):
        return self.config['sensors']

    @property
    def actors(self):
        return self.config['actors']

    @property
    def hash(self):
        return hash(tuple(self.config))

    def _merge_sensors(self, sensors):
        for sensor in self.config['sensors']:
            sensor['delete'] = True

        for candidate in sensors:
            merged = False

            for sensor in self.config['sensors']:
                if candidate['id'] == sensor['id']:
                    sensor['name'] = candidate['name']
                    sensor['type'] = candidate['type']
                    sensor['config'] = candidate['config']
                    sensor['delete'] = False
                    merged = True
                    break

            if merged is False:
                self.config['sensors'].append({
                    'id': candidate['id'],
                    'name': candidate['name'],
                    'type': candidate['type'],
                    'config': candidate['config']
                })

    def _merge_actors(self, actors):
        for actor in self.config['actors']:
            actor['delete'] = True

        for candidate in actors:
            merged = False

            for actor in self.config['actors']:
                if candidate['id'] == actor['id']:
                    actor['name'] = candidate['name']
                    actor['type'] = candidate['type']
                    actor['config'] = candidate['config']
                    actor['delete'] = False
                    merged = True
                    break

            if merged is False:
                self.config['actors'].append({
                    'id': candidate['id'],
                    'name': candidate['name'],
                    'type': candidate['type'],
                    'config': candidate['config'],
                    'delete': False
                })

    def merge(self, file_config):
        self._merge_sensors(file_config.sensors)
        self._merge_actors(file_config.actors)

    def save(self):
        for sensor in self.config['sensors']:
            if 'delete' in sensor and sensor['delete'] is True:
                self.connector.deleteSensor(sensor['id'])
            else:
                self.connector.saveSensor(sensor)

        for actor in self.config['actors']:
            if 'delete' in actor and actor['delete'] is True:
                self.connector.deleteActor(actor['id'])
            else:
                self.connector.saveActor(actor)
