# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


import json
import os
from datetime import datetime

from dateutil.parser import parse
from tzlocal import get_localzone


class FileConfiguration(object):

    def __init__(self, filename):

        """
            if not isfile(config_filename):
                logging.error("config.json not found. exiting.")
                return False

            try:
                with open(config_filename) as data_file:
                    file_config = json.load(data_file)
            except ValueError as e:
                logging.error("Configuration file could not be loaded error is:")
                logging.error(e)
                return False

            file_modified_time = os.path.getmtime(config_filename)
            logging.info('File was last modified at %s' % file_modified_time)

            logging.info(file_config)

        """

        self.filename = filename

        try:
            with open(self.filename) as f:
                self.config = json.load(f)

            tz = get_localzone()
            self.last_change = tz.localize(datetime.fromtimestamp(os.path.getmtime(filename)))

        except (IOError, ValueError) as e:
            raise IOError(e)

    @property
    def id(self):
        return self.config['id']

    @property
    def uuid(self):
        return self.config['uuid']

    @uuid.setter
    def uuid(self, value):
        self.config['uuid'] = value

    @property
    def sensors(self):
        return self.config['sensors']

    @property
    def actors(self):
        return self.config['actors']

    @property
    def host(self):
        return self.config['connection']['host']

    @property
    def port(self):
        return self.config['connection']['port']

    @property
    def api_key(self):
        return self.config['connection']['api-key']

    @api_key.setter
    def api_key(self, value):
        self.config['connection']['api-key'] = value

    @property
    def connection(self):
        return self.config['connection']

    @property
    def last_merge(self):
        return parse(self.config['last_merge']) if 'last_merge' in self.config else None

    @last_merge.setter
    def last_merge(self, last_merge):
        self.config['last_merge'] = last_merge.isoformat()

    @property
    def hash(self):
        return self.config['hash']

    def merge(self, server_config):
        self.config['hash'] = server_config.hash
        self.last_merge = server_config.last_change
        self._merge_sensors(server_config.sensors)
        self._merge_actors(server_config.actors)

    def save(self):
        f = open(self.filename, 'w+')
        json.dump(self.config, f, indent=4)

    def _merge_sensors(self, sensors):
        for sensor in self.config['sensors']:
            sensor['delete'] = True

        for candidate in sensors:
            merged = False

            for sensor in self.config['sensors']:
                if candidate['id'] == sensor['id']:
                    sensor['name'] = candidate['name']
                    sensor['type'] = candidate['type']
                    sensor['config'] = candidate['config']
                    sensor['delete'] = False
                    merged = True
                    break

            if merged is False:
                self.config['sensors'].append({
                    'id': candidate['id'],
                    'name': candidate['name'],
                    'type': candidate['type'],
                    'config': candidate['config'],
                    'delete': False,
                    'monitor': True
                })

        self.config['sensors'] = [sensor for sensor in self.sensors if sensor['delete'] is False]
        for sensor in self.config['sensors']:
            sensor.pop('delete', None)

    def _merge_actors(self, actors):
        for actor in self.config['actors']:
            actor['delete'] = True

        for candidate in actors:
            merged = False

            for actor in self.config['actors']:
                if candidate['id'] == actor['id']:
                    actor['name'] = candidate['name']
                    actor['type'] = candidate['type']
                    actor['config'] = candidate['config']
                    actor['delete'] = False
                    merged = True
                    break

            if merged is False:
                self.config['actors'].append({
                    'id': candidate['id'],
                    'name': candidate['name'],
                    'type': candidate['type'],
                    'config': candidate['config'],
                    'delete': False
                })

        self.config['actors'] = [actor for actor in self.actors if actor['delete'] is False]
        for actor in self.config['actors']:
            actor.pop('delete', None)

