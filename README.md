# farmee controller
This is the software for running a grow system with a Raspberry PI.
We call this grow system "unit".

## Installation
See INSTALL.md for installation instructions.

## Troubleshooting
If you have problems with the controller, please check the logfile that is written
as .log in your installation folder. If you still don't see any problem try
to stop the service with `sudo systemctl stop farmee.service` and start the python
script manually with active debugging: `sudo python main.py --loglevel=debug`

## Technical overview
There are several available Classes to read sensors and use actors.

### Configuration:
Upon the first start you will be asked for an API key and an unique unit identifier.
The identifier needs to be unique among your systems if you use multiple systems.
To obtain an API key, visit http://brain.farmee.io

### Available Sensors
* DHT22 - Temperature and Humidity. Needs Adafruit drivers to be installed (see
  sensors/DHT22.py for details)
* TSL2561 - Lux Sensor (I2C Bus)
* AtlasPH - Atlas Scientific PH Sensor (I2C Bus)
* AtlasEC - Atlas Scientific EC Sensor (I2C Bus)
* YoctoCO2 - Yoctopuce CO2 Sensor
* WaterlevelSimple - A simple Input for monitoring a waterlevel (GPIO)

### Available Logic Elements
* Regulator - Regulates a value to a target via an actor and a sensor

### Available Actors
* YoctoMaxiPowerRelay - Yoctopuce Maxi Power Relay
* LightSimple - Switch
* WaterSimple - Switch
* FanSimple - Switch

All three "Simple" actors implement the exact same functionality, to switch a GPIO
port to high or low. Via the parameter switchlevel it can be decided if the
actor should switch to high when its state is "on" or switch to low. The GPIO
port is pulled to high/low depending on the switchlevel, to have clean signals.

### Timers
There are two different methods of controlling actors over time, switch and
trigger.

The switch switches an actor to a state over a time. The best example for this
would be a light, that is turned on in the morning and turned off in the
evening. So the switch definition is sth. like "switch actor light from 5am to
9pm to state on".

The trigger switches an actor to a defined state for a defined time at a defined
time. The definition when to switch is made by an crontab like notation with
hours and minutes. `hours=*,minutes=*/2` would resolve to: trigger every hour
and everytime the current minute is dividable by 2. As a second parameter the
switchtime is defined, e.g. 10 seconds.

### Autostart / Daemon
To install the autostart, edit the autostart/farmee.service file to your needs 
and then copy autostart/farmee.service to /lib/systemd/system.
After that restart the daemon with `systemctl daemon-reload` and enable the 
service with `systemctl enable farmee.service`.
To manually start the service use `systemctl start farmee.service`.
