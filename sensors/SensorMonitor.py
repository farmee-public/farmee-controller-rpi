# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


class SensorMonitor(object):
    def __init__(self):
        self.sensor = False
        self.value = False
        self.name = ''
        self.sensorId = ''

    def init(self, sensor, value, name, sensorId):
        self.sensor = sensor
        self.value = value
        self.name = name
        self.sensorId = sensorId
        return

    def read(self):
        return self.sensor.read(self.value)

    def getName(self):
        return self.name

    def getValue(self):
        return self.value

    def getSensorId(self):
        return self.sensorId
