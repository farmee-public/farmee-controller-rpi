# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


import smbus
from sensors.SensorBase import SensorBase
import logging

class TSL2561(SensorBase):

    def __init__(self,name):
        super(TSL2561,self).__init__(name)
        self.bus_number = 1
        self.bus_address = 0x39 # decimal 57

        self.values = { "ir": 0, "ambient": 0 }

        return

    def init(self,payload):
        self.bus_address = int(float(payload["i2c-address"]))

        # create i2c object
        self.i2cBus = smbus.SMBus(self.bus_number)

    def getType(self):
        return "TSL2561"

    def read(self,value):
        return self.values[value]
    
    def _refresh(self):

        # start measurement
        self.i2cBus.write_byte_data(self.bus_address, 0x80, 0x03)

        # read ambient data
        ambientLow = self.i2cBus.read_byte_data(self.bus_address, 0x8c)
        ambientHigh = self.i2cBus.read_byte_data(self.bus_address, 0x8d)
        ambient = (ambientHigh*256)+ambientLow

        # read ir data
        irLow = self.i2cBus.read_byte_data(self.bus_address, 0x8e)
        irHigh = self.i2cBus.read_byte_data(self.bus_address, 0x8f)
        ir = (irHigh*256)+irLow

        # calculate ir/ambient ratio
        if ambient > 0:
            ratio = ir / float (ambient)
        else:
            ratio = 0

        # calculation according to TSL2561 data sheet
        if 0 < ratio <= 0.50:
            lux = 0.0304*ambient-0.062*ambient*(ratio**1.4)
        elif 0.50 < ratio <= 0.61:
            lux = 0.0224*ambient-0.031*ir
        elif 0.61 < ratio <= 0.80:
            lux = 0.0128*ambient-0.0153*ir
        elif 0.80 < ratio <= 1.3:
            lux = 0.00146*ambient-0.00112*ir
        else:
            lux = 0
        
        self.values = { "ir": ir, "ambient": lux }
        
        logging.info( "read TSL2561 %s", self.values )
        return True
    
    

    
