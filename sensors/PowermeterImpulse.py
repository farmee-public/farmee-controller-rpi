# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


from sensors.SensorBase import SensorBase
from time import sleep

import time
import RPi.GPIO as GPIO
import logging


class PowermeterImpulse(SensorBase):
    """
    Measuring power consumption via impulses on a gpio pin
    default 1000 impulses per kwh
    output is in wh
    the input is pulled to ground, so the impulses have to be + impulses
    """

    pin = 0
    impulsesPerKwh = 1000
    lastValue = 0
    impulseCount = 0
    startTime = 0
    updateTime = 0

    last1MinVal = 0
    last1MinWh = 0
    last5MinVal = 0
    last5MinWh = 0
    last15MinVal = 0
    last15MinWh = 0
    last60MinVal = 0
    last60MinWh = 0

    values = {"wh": 0, "wh1min": 0, "wh5min": 0, "wh15min": 0, "wh60min": 0}

    def read(self, value):
        logging.debug( "powermeter value %s", self.values["wh"])
        return self.values[value]

    def getType(self):
        return "Powermeter"

    def init(self,payload):
        self.pin = payload[ "gpio-pin" ]
        GPIO.setup( self.pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN )
        self.startTime = time.time()
        return

    def _refresh(self):
        currentTime = int(time.time())
        timediff = int(round( currentTime - self.startTime ))

        # logging.info( "time %s %s", currentTime, self.updateTime )
        # logging.info( "timediff %s", timediff )

        # @todo calculate with impulses per kwh
        wh = self.impulseCount

        if( currentTime > self.updateTime ):
            # logging.info( "check %s %s", timediff, ( timediff % 60 == 0 ) )
            # logging.info( "check %s", ( timediff % 60 ) == 0 )
            self.updateTime = currentTime

            if( timediff % 60 == 0 ):
                self.last1MinVal = wh - self.last1MinWh
                self.last1MinWh = wh
                logging.info( "update last 1 min val: %s %s", wh, self.last1MinVal )

            if( timediff % 300 == 0 ):
                self.last5MinVal = wh - self.last5MinWh
                self.last5MinWh = wh
                logging.info( "update last 5 min val: %s %s", wh, self.last5MinVal )

            if( timediff % 900 == 0 ):
                 self.last15MinVal = wh - self.last15MinWh
                 self.last15MinWh = wh

            if( timediff % 3600 == 0 ):
                 self.last60MinVal = wh - self.last60MinWh
                 self.last60MinWh = wh

        val = GPIO.input(self.pin)
        if val == 1 and self.lastValue == 0:
            self.impulseCount = self.impulseCount+1
        self.lastValue = val

        self.values = {
            "wh": wh,
            "wh1min": self.last1MinVal,
            "wh5min": self.last5MinVal,
            "wh15min": self.last15MinVal,
            "wh60min": self.last60MinVal,
        }
        return True

    def run(self):
        logging.debug("Starting powermeter thread")
        while True:
            self.refresh()
            sleep(0.09)
