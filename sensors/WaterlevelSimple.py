# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


from sensors.SensorBase import SensorBase

import RPi.GPIO as GPIO
import logging

class WaterlevelSimple(SensorBase):

    pin = 0
    values = { "low": 0 }

    def read(self,value):
        return self.values[value]

    def getType(self):
        return "WaterlevelSimple"

    def init(self,payload):
        self.pin = payload[ "gpio-pin" ]
        GPIO.setup( self.pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN )
        return

    def _refresh(self):
        val = GPIO.input( self.pin )
        self.values = { "low": val }
        return True
