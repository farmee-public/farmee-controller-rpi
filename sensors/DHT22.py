# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


import Adafruit_DHT
from sensors.SensorBase import SensorBase
import RPi.GPIO as GPIO
import logging




class DHT22(SensorBase):
    """
    Class to use the Adafruit DHT22 Sensor

    Requires installed Adafruit DHT22 drivers. To install follow these steps:
      git clone https://github.com/adafruit/Adafruit_Python_DHT.git
      cd Adafruit_Python_DHT
      sudo python setup.py install

    """

    def __init__(self, name):
        super(DHT22, self).__init__(name)
        self.sensortype = 22
        self.pin = 0
        self.values = {"humidity": 0, "temperature": 0}

    def getType(self):
        return "DHT22"

    def init(self,payload):
        self.pin = payload["gpio-pin"]
        logging.info("Initializing DHT22 on pin %s", self.pin)
        GPIO.setup(self.pin, GPIO.IN)

    def read(self, value):
        return self.values[value]

    def _refresh(self):
        logging.debug('start read DHT22 on pin %s', self.pin )
        humidity, temperature = Adafruit_DHT.read_retry(self.sensortype, self.pin)
        self.values["humidity"] = humidity
        self.values["temperature"] = temperature

        logging.debug('read DHT22 %s', self.values)

