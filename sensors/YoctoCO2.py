# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


from sensors.SensorBase import SensorBase

from lib.yocto.yocto_carbondioxide import *

import logging

class YoctoCO2(SensorBase):
    """
    Class to integrate a Yoctopuce CO2 sensor
    """

    values = { "co2": 0 }
        
    def read(self,value):
        return self.values[value]
    
    def getType(self):
        return "YoctoCO2"
    
    def init(self,payload):
        self.serial = payload["serial"]
        self.sensor = YCarbonDioxide.FindCarbonDioxide(self.serial + '.carbonDioxide')

        if not (self.sensor.isOnline()):
            logging.error('Yocto CO2 sensor %s not connected',self.serial)

        return
    
    def _refresh(self):
        co2val = 0
        if not (self.sensor.isOnline()):
            logging.error('Yocto CO2 sensor %s not connected',self.serial)
        else:
            co2val = self.sensor.get_currentValue()

        self.values = { "co2": co2val }
        return True
