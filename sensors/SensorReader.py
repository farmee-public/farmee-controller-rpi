# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


import logging


class SensorReader(object):
    def __init__(self):
        self.outputs = []
        self.sensor = False
        self.value = False

    def init(self, sensor, value):
        self.sensor = sensor
        self.value = value
        return

    def addOutput(self, o):
        self.outputs.append(o)
        return

    def triggerOuts(self):
        v = self.sensor.read(self.value)
        logging.debug("read sensor %s value -> %d", self.sensor.getType(), v)
        for o in self.outputs:
            o.input(v)
