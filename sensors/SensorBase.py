# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


import sys
from abc import ABCMeta, abstractmethod
from time import sleep
from threading import Thread
import logging


class SensorBase(Thread):
    """
    this is the base class for implementing sensors.
    each sensor should extend this class. this class itself extends
    the Thread to run every sensor reading in separate threads that
    do not interfere with the main logic.
    """

    __metaclass__ = ABCMeta
    _stop = False

    def __init__(self, name):
        super(SensorBase, self).__init__()
        self.setDaemon(True)
        self.name = name

    def run(self):
        logging.debug("start thread")
        while True and self._stop == False:
            self.refresh()
            sleep(5)

        logging.debug("stop thread")
        return

    def stop(self):
        self._stop = True

    @abstractmethod
    def _refresh(self):
        """
        refresh reads the values from the actual sensor hardware and
        stores them in the sensor instance; if not defined otherwise
        in the run method, refresh will be called every second
        """
        pass

    def refresh(self):
        try:
            self._refresh()
        except IOError as e:
            logging.error("Could not refresh sensor '{}' ** : '{}'".format(self.name, e))

    '''
    read shoud get the stored values, that have been read by the
    refresh method. refresh MUST be called at least once before read
    can return a value. for special cases, the value can be read
    from the sensor directly in the read method, but it is not
    recommended due to latencies that can occur.
    '''
    @abstractmethod
    def read(self,value):
        pass

    '''
    getType identifies the sensor type (e.g. DHT22)
    '''
    @abstractmethod
    def getType(self):
        pass

    '''
    init initializes the sensor. payload is an dictionary with all the
    values, that are defined in the config.json for the sensor. so
    each sensor has to know how to deal with the passed information.
    '''
    @abstractmethod
    def init(self,payload):
        pass
