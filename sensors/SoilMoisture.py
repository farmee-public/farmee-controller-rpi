# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


import logging
from collections import deque

import Adafruit_MCP3008
import RPi.GPIO as GPIO

from sensors.SensorBase import SensorBase


class SoilMoisture(SensorBase):
    """
    Class for using the SparkFun Soil Moisture Sensor via Adafruit MCP3008
    """

    def init(self,payload):
        # Init Adafruit MCP3008
        self.clk = payload[ "gpio-clk" ]
        self.miso = payload[ "gpio-miso" ]
        self.mosi = payload[ "gpio-mosi" ]
        self.cs = payload[ "gpio-cs" ]
        self.act = payload[ "gpio-act" ]
        self.mcp = Adafruit_MCP3008.MCP3008(clk=self.clk, cs=self.cs, miso=self.miso, mosi=self.mosi)

        self.values = {"m": 0}

        # Init activating GPIO for the sensor
        GPIO.setup(self.act, GPIO.OUT)
        GPIO.output(self.act, GPIO.LOW)

        self.q = deque()

    def _refresh(self):
        # Read from Adafruit MCP3008
        GPIO.output(self.act, GPIO.HIGH)
        value = self.mcp.read_adc(0)
        GPIO.output(self.act, GPIO.LOW)

        # Put value into the queue, remove one item if full
        if len(self.q) >= 10:
            self.q.popleft()
        self.q.append(value)
        logging.debug( "m values %s", value )
        logging.debug( "m read %s", self.q )
        
    def read(self,value):
        sum = 0
        for v in self.q:
            sum = sum + v
        avg = sum / len(self.q)
        logging.debug( "m avg %s from %s", avg, self.q )
        return avg
    
    def getType(self):
        return "SoilMoisture"
