# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


from sensors.SensorBase import SensorBase

import spidev

class CO2(SensorBase):
    
    sensortype = 22
    adcpin = 2
    spi = spidev.SpiDev()
    
    values = { "co2": 0, "volt": 0 }
        
    def read(self,value):
        return self.values[value]
    
    def getType(self):
        return "CO2"
    
    def init(self,payload):
        self.adcpin = payload[ "adcpin" ]
        self.spi.open(0,0)
        return
    
    def _refresh(self):
        co2val = self.readadc( self.adcpin )
        co2volts = ( co2val * 3.3 ) / 1024
        self.values = { "co2": co2val, "volt": co2volts }
        return True

    def readadc(self,adcnum):
        if adcnum >7 or adcnum <0:
            return -1
        r = self.spi.xfer2([1,8+adcnum <<4,0])
        adcout = ((r[1] &3) <<8)+r[2]
        return adcout
