# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


import io         # used to create file streams
import fcntl      # used to access I2C parameters like addresses

import time       # used for sleep delay and timestamps
import string     # helps parse strings

from sensors.SensorBase import SensorBase
import logging

class AtlasPH(SensorBase):
    """
    Class for using the Atlas Scientific PH Probe with the Atlas PH EZO Board

    This class is based on the code from Atlas Scientific provided with this
    repository: https://github.com/AtlasScientific/Raspberry-Pi-sample-code
    """

    def __init__(self, name):
        super(AtlasPH, self).__init__(name)
        
        self.long_timeout = 1.5           # the timeout needed to query readings and calibrations
        self.short_timeout = .5           # timeout for regular commands
        self.default_bus = 1              # the default bus for I2C on the newer Raspberry Pis, certain older boards use bus 0
        self.default_address = 99         # the default address for the sensor (0x63)

        self.values = { "ph": 0 }
        return

    def init(self, payload):
        bus = self.default_bus
        address = int(float(payload["i2c-address"]))
        logging.debug("Initialize PH with address %s", address)
        
        # open two file streams, one for reading and one for writing
        # the specific I2C channel is selected with bus
        # it is usually 1, except for older revisions where its 0
        # wb and rb indicate binary read and write
        self.file_read = io.open("/dev/i2c-"+str(bus), "rb", buffering=0)
        self.file_write = io.open("/dev/i2c-"+str(bus), "wb", buffering=0)

        # initializes I2C to either a user specified or default address
        self.set_i2c_address(address)

    def set_i2c_address(self, addr):
        # set the I2C communications to the slave specified by the address
        # The commands for I2C dev using the ioctl functions are specified in
        # the i2c-dev.h file from i2c-tools
        I2C_SLAVE = 0x703
        fcntl.ioctl(self.file_read, I2C_SLAVE, addr)
        fcntl.ioctl(self.file_write, I2C_SLAVE, addr)

    def writei2c(self, cmd):
        # appends the null character and sends the string over I2C
        cmd += "\00"
        self.file_write.write(cmd)

    def readi2c(self, num_of_bytes=31):
        # reads a specified number of bytes from I2C, then parses and displays the result
        res = self.file_read.read(num_of_bytes)         # read from the board
        response = filter(lambda x: x != '\x00', res)     # remove the null characters to get the response
        if ord(response[0]) == 1:             # if the response isn't an error
            # change MSB to 0 for all received characters except the first and get a list of characters
            char_list = map(lambda x: chr(ord(x) & ~0x80), list(response[1:]))
            # NOTE: having to change the MSB to 0 is a glitch in the raspberry pi, and you shouldn't have to do this!
            return ''.join(char_list)     # convert the char list to a string and returns it
        else:
            return "Error " + str(ord(response[0]))

    def query(self, string):
        # write a command to the board, wait the correct timeout, and read the response
        self.writei2c(string)

        # the read and calibration commands require a longer timeout
        if((string.upper().startswith("R")) or
           (string.upper().startswith("CAL"))):
            time.sleep(self.long_timeout)
        elif string.upper().startswith("SLEEP"):
            return "sleep mode"
        else:
            time.sleep(self.short_timeout)

        return self.readi2c()

    def close(self):
        self.file_read.close()
        self.file_write.close()

    def _refresh(self):
        val = self.query("R")
        logging.debug("ph read %s", val)
        if is_number(val):
            self.values["ph"] = val

    def read(self,value):
        return self.values[value]
    
    def getType(self):
        return "PH"


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
