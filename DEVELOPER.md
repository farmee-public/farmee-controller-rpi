## Sensors/Monitors
A sensor can be monitored or unmonitored. Monitored sensors are written to the
unitstate. Unmonitored sensors can be used for logic elements or warnings but
will not be written to the unitstate. For monitoring a sensor, a SensorMonitor
is initialized and connected to the Sensor. Therefore every sensor class has to
implement a read method. A SensorMonitor reads exactly one value from a sensor.
A sensor itself can read multiple values and store them for further reading. If
a sensor reads multiple values (e.g. DHT22 reads temperature and humidity), then
multiple monitors will be initialized for the same sensor.

## Actors
In general there is only one type of actor: Switching a GPIO port to high or
low. Everything of the logic happens in the SwitchGPIO class. There are two
derrived classes LightSimple and WaterSimple. The only point to have these two
classes is to have "typed" actors to identify what function (watering, lighting,
etc.) an actor fullfils.

The actors have an input function, from that they get their trigger value, right
now this is only true/false, in the future the value could be more complex. The
basic idea is the modeling of a mini-graph, so outputs are read and given to
inputs of actors. The actor then decides what to do with the input. Right now
the timers send true or false to the input of the actors and the actors then
toggle their GPIO port.

## Timers
There are two timer elements: TimerSwitch and TimerTrigger. Both of them are
initialized via the TimerRunner. The TimerRunner creates a new thread, so the
time handling is decoupled from the execution of the rest of the application.
The initialization of the TinmerRunner is called with the list of actor
instances, so each timer that is created will directly be connected to its
designated actor instance and sends a value (true or false, depending on the
current time) to the input function of the actor

## Add new sensors
All sensors should extend the SensorBase class and implement the abstract
methods from this class. See the documentation of this class for further
details. The sensor class should then be placed in the sensors folder. Then the
sensor can be initialized via the control.json. The sensor name in the json has
to be the name of the class. E.g. sensor name in control.json "AtlasPH" results
in creating a new instance of the class AtlasPH found in sensors/AtlasPH.py.
