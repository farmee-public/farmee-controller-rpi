# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


import datetime
import logging
from timers.TimerBase import TimerBase


class TimerSwitch(TimerBase):

    def __init__(self):
        super(TimerSwitch, self).__init__()
        self.timeFrom = False
        self.timeTo = False

    def setTimeConfig(self, tFrom, tTo):
        partsFrom = tFrom.split(":")
        self.timeFrom = datetime.time(int(partsFrom[0]), int(partsFrom[1]))

        partsTo = tTo.split(":")
        self.timeTo = datetime.time(int(partsTo[0]), int(partsTo[1]))

        logging.info("set time config " + tFrom + " " + tTo)

    def trigger(self):
        currentTime = datetime.datetime.now().time()

        if self.timeFrom < currentTime < self.timeTo:
            state = True
            # logging.debug( "timecheck positive -> switch is on")
        else:
            state = False
            # logging.debug( "timecheck negative -> switch is off")

        self.propagateValueToOutputs(state)
