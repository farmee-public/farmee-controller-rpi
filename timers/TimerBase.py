# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


import logging


class TimerBase(object):
    
    def __init__(self):
        self.outputs = []
    
    def addOutput(self,o):
        logging.debug( "add output to %s", self )
        self.outputs.append( o )
        return

    def propagateValueToOutputs(self,v):
        logging.debug( "trigger outputs of %s", self )
        for o in self.outputs:
            o.input( v )
        return
