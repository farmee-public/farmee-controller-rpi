# -*- coding: utf-8 -*-
#
#   farmee controller
#   Copyright (C) 2018  farmee GmbH
#
#   This file is part of farmee controller.
#
#   farmee controller is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   farmee controller is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with farmee controller.  If not, see <http://www.gnu.org/licenses/>.


from timers.TimerBase import TimerBase
import logging
import time


class TimerInterval(TimerBase):
    
    def __init__(self):
        super(TimerInterval, self).__init__()
        self.hour = '0'
        self.minute = '0'
        self.second = '0'
        self.duration = 0
        self.endTime = -1
    
    def setTime(self, h, m, s='0'):
        # logging.info("set timertrigger time %s %s %s", h, m, s )
        self.hour = h
        self.minute = m
        self.second = s
    
    def setDuration(self, d):
        self.duration = int(d)
    
    def trigger(self):
        state = self.__check()
        # logging.debug( 'time trigger check %s', state )
        self.propagateValueToOutputs(state)
    
    def __check(self):
        if self.endTime != -1 and time.time() <= self.endTime:
            logging.debug('active due to endtime %s', self.endTime)
            return True
        
        h = time.strftime('%H')
        m = time.strftime('%M')
        s = time.strftime('%S')
        
        hourCheck = self.__checkValue(self.hour, h)
        minuteCheck = self.__checkValue(self.minute, m)
        secondCheck = self.__checkValue(self.second, s)
        logging.debug( 'timer check: %s %s %s -> %s %s %s', h, m, s, hourCheck, minuteCheck, secondCheck)
        
        check = False
        if hourCheck and minuteCheck:
            if secondCheck is True:
                check = True
            elif self.second.isdigit() and self.duration != 0:
                if int(s) < int(self.second) + self.duration:
                    check = True
            
        if check is True and self.duration != 0:
            self.endTime = time.time() + self.duration

        return check

    def __checkValue(self, valueSet, valueCheck):
        valueCheck = int(valueCheck)
        if valueSet == '*':
            return True
        elif str(valueSet)[0:2] == '*/':
            v = int(valueSet[2:])
            if valueCheck % v == 0:
                return True
        elif ',' in str(valueSet):
            values = valueSet.split(',')
            if str(valueCheck) in values:
                return True
        elif int(valueSet) == valueCheck:
            return True
        
        return False
